=== rooten ===

Contributors: bdthemes
Tags: translation-ready, custom-background, theme-options, custom-menu, post-formats, threaded-comments

Requires at least: 4.4
Tested up to: 4.8.1
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Rooten Theme designed by BdThemes Ltd.  Specially designed for Elementor Page builder based website. it's so much easy and fast.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Rooten support those plugins:
1. Elementor Page Builder
2. Slider Revolution
3. Rooten Core Plugin
3. The Event Calender
4. Services Plugin
5. Assistant Plugin
6. BdThemes FAQ
7. Booked Calender
8. BdThemes Testimonial


== Change-log ==

= 1.2.6 - 4 March 2019 =
# woocommerce cart link depricated issue fixed
# woocommerce version warning issue fixed

= 1.2.5 - 1 February 2019 =
+ Portfolio template added (now bdtheme-portfolio plugin will work perfectly)
+ Booked Calendar plugin added
# underline issue fixed for all anchor tag

= 1.2.4 - 15 January 2019 =
# update not working issue fixed

= 1.2.3 - 11 September 2018 =
^ Fullwidth page now default for better start with elementor 
# dropdown menu color fixed
# minor css issue fixed 

= 1.2.1 - 01 February 2018 =
# WPML Dropdown language switcher issue fixed
# Menu style firefox browser issue fixed
# Button hover, active issue fixed for customizer
^ Improved some minor issue for css style


= 1.2.0 - 17 December 2017 =
+ Brand new demo importer with separated demo installation option added.
^ Offcanvas color now you can change from customizer
^ UiKit Updated to latest version
^ Assistant now added more options for control grid, carousel view in elementor.
^ Service now added more options for control grid, carousel view in elementor.
^ Many small improvement we did.


= 1.1.1 - 11 September 2017 =
# Assistant category page sidebar fixed
# Services category page sidebar fixed
+ Mobile Offcanvas menu background change option added
# Fix services grid lightbox issue
# Fixed some minor issues in CSS style
+ Booked widget added
# Fix some options in contact form 7 widget
# Fix Testimonials carousel widget option
# Fix Testimonials Slider widget option

= 1.1.0 - 01 September 2017 =
+ Flip box element added
+ Panel Slider element added
+ Contact Form 7 element added
+ Image Compare element added
+ Trailer Box element added
+ Instagram element added
# Assistant wrong view in elementor fixed
# Woocommerce Image gallery fixed
# Image Gallery lightbox issue fixed
# Elementor back to wrong page fixed
# Fixes some minor issues in Javascript and CSS
# Refine some coding structure
$ Language file updated

= 1.0.0 - 24 August 2017 =
* Initial release

== Credits ==

* Based on Underscores http://underscores.me/, (C) 2012-2016 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css http://necolas.github.io/normalize.css/, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](http://opensource.org/licenses/MIT)
