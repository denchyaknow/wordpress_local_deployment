<?php
$social_link = get_post_meta( get_the_ID(), 'bdthemes_portfolio_social_link', true );
if($social_link != null and is_array($social_link)) : ?>
	<div class="bdt-portfolio-social bdt-position-absolute bdt-position-bottom-center">
		<ul class="bdt-list bdt-margin-remove-bottom">
	    <?php foreach ($social_link as $link) : ?>
	        <?php $tooltip = ucfirst(rooten_helper::icon($link)); ?>
	        <li class="bdt-display-inline-block">
	            <a<?php echo rooten_helper::attrs(['href' => $link, 'class' => 'bdt-margin-small-right']); ?> bdt-icon="icon: <?php echo rooten_helper::icon($link); ?>" title="<?php echo esc_html($tooltip); ?>" bdt-tooltip></a>
	        </li>
	    <?php endforeach ?>
	    </ul>
	</div>
<?php endif; ?>