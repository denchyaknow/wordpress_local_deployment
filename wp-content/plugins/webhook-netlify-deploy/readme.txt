=== Deploy Netlify Webhook ===
Contributors: lukethacoder
Donate link: https://www.buymeacoffee.com/lukesecomb
Tags: webhook, netlify, deploy, gatsbyjs, gatsby, static, build, staticsite, buildtool, ci
Stable tag: trunk
Requires at least: 4.4.0
Requires PHP: 5.2.4
Tested up to: 5.2.1
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Easily deploy static sites using Wordpress and Netlify

== Description ==

Easily deploy static sites using Wordpress and Netlify

**Build** Quickly and easily send webhooks to build your Netlify

**Status** Check the status of your latest build to see if it was successful without even leaving Wordpress

= Links =
* [Documentation](https://github.com/lukethacoder/deploy-webhook-button)
* [Github](https://github.com/lukethacoder/deploy-webhook-button)
* [Author](https://lukesecomb.digital)


== Installation ==

From your WordPress dashboard

1. **Visit** Plugins > Add New
2. **Search** for "Deploy Netlify Webhook"
3. **Activate** "Deploy Netlify Webhook" from your Plugins page
4. **Click** on the new menu item "Deploy Netlify Webhook" and enter your site details/keys
5. **Enter** enter your site_id, webhook POST address, Netlify API Key, and User-Agent
6. **Read** the documentation to [get started](https://github.com/lukethacoder/wp-webhook-netlify-deploy)


== Screenshots ==

1. Developer Settings Page.
2. User Settings Page.

== Frequently Asked Questions ==

= I have an issue with my build? =

Double check the netlify docs. this tool is to send a reqest to build your site, Netlify does all the heavy lifting.

== Upgrade Notice ==

= 1.0.0 =
Seperate Developer Settings and User Build Screen allows users and developers to see different settings.

== Changelog ==

= 1.0.1 =
* Updated Documentation
* Updated plugin screenshots

= 1.0.0 =
* Fixed UI
* Seperate Developer Settings and User Build Screen

= 0.1.0 =
* Initial Release

View full changelog: https://github.com/lukethacoder/deploy-webhook-button